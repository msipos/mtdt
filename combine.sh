#!/bin/bash

cp dist/mtdt.jar combine/mtdt.jar
cd combine/
java -jar proguard.jar @proguard.conf
chmod +x ./out.jar
cp ./out.jar ~/mtdt.jar
