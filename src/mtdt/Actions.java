package mtdt;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;
import mtdt.model.Item;
import mtdt.model.Model;

/** Common actions. All of these methods are executed by MainWindow on EDT. */
public class Actions {
  public static void newDb(File rootDir) {
    try {
      final String rootPath = rootDir.getAbsolutePath();
      Model.init(rootPath);
      Mtdt.mw.switchToList();
      Mtdt.prefs.setLastDb(rootPath);
      Actions.updateListModel();
    } catch (IOException ex) {
      Mtdt.mw.warning("IO Error while creating/opening database: " + ex.getMessage());
    }
  }


  public static void scanDb() {
    String rootPath = Model.getRootDir();
    if (rootPath == null) {
      Mtdt.mw.warning("You must first create or open a database!");
    } else {
      try {
        Scanner.scan(new File(rootPath));
      } catch (IOException ex) {
        Mtdt.mw.warning("IO Error while scanning: " + ex.getMessage());
      }
    }
    updateListModel();
  }


  public static void updateListModel() {
    // Create matchers:
    final String searchField = Mtdt.mw.getSearchText();
    final String[] matchers = searchField.split("\\s+");
    final ArrayList<String> cleanMatchers = new ArrayList<String>();
    for (String matcher : matchers) {
      if (matcher != null && !matcher.equals("")) {
        cleanMatchers.add(matcher.toLowerCase());
      }
    }

    System.out.println("Updating list model: " + cleanMatchers.size() + " matchers");


    // First populate the temporary array and sort it:
    ArrayList<Item> tempItems = new ArrayList<Item>();
    for (Item i : Model.getAllItems()) {
      if (i.match(cleanMatchers)) tempItems.add(i);
    }
    Collections.sort(tempItems);

    // Finally update the actual list model:
    DefaultListModel<Item> itemsListModel = Mtdt.mw.getItemListModel();
    itemsListModel.clear();
    for (Item i : tempItems) {
      itemsListModel.addElement(i);
    }
  }


  /** User has picked the i-th element in the listModel. */
  public static void pickedListItem(int i) {
    DefaultListModel<Item> itemsListModel = Mtdt.mw.getItemListModel();
    Item item = itemsListModel.get(i);
    Model.setCurrentItem(item);
    Mtdt.mw.setSubtitle(item.getKey());

    // Erase the properties table:
    DefaultTableModel tableModel = Mtdt.mw.getPropTableModel();
    for (int j = tableModel.getRowCount() - 1; j >= 0; j--) {
      tableModel.removeRow(j);
    }

    // Populate the properties table:
    final String[] props = item.getProps(), values = item.getValues();
    for (int j = 0; j < props.length; j++) {
      tableModel.addRow(new String[] { props[j], values[j] } );
    }

    Mtdt.mw.activatePropTableChanges();
    Mtdt.mw.switchToProp();
  }


  /** User has picked the back button (or pressed Escape). */
  public static void backToMain() {
    Model.setCurrentItem(null);
    Mtdt.mw.switchToList();
    Mtdt.mw.deactivatePropTableChanges();

    updateListModel();
  }


  /** User has picked the show button. */
  public static void showCurrentItem() {
    final Item item = Model.getCurrentItem();
    if (item == null) {
      System.err.println("There has been an error: user managed to click Show but there is no Item.");
      return;
    }
    final File f = item.getFile();
    try {
      Desktop.getDesktop().open(f);
    } catch (Exception ex) {
      Mtdt.mw.warning("There has been an error while trying to open:\nFile '" + f.getAbsolutePath() + "':\n" + ex);
    }
  }


  /** User has changed the property table. */
  public static void propTableChanged() {
    final DefaultTableModel tableModel = Mtdt.mw.getPropTableModel();
    final int numRows = tableModel.getRowCount();

    // Costruct arrays:
    final String[] props = new String[numRows], values = new String[numRows];
    for (int row = 0; row < numRows; row++) {
      props[row] = (String) tableModel.getValueAt(row, 0);
      values[row] = (String) tableModel.getValueAt(row, 1);
    }

    Model.changeCurrentItemsProps(props, values);
    try {
      Model.commit();
    } catch (IOException ex) {
      Mtdt.mw.warning("IOError while updating record: " + ex);
    }
  }


  /** User has chosen to delete this item. */
  public static void delete() {
    Model.deleteCurrentItem();
    try {
      Model.commit();
    } catch (IOException ex) {
      Mtdt.mw.warning("IOError while deleting record: " + ex);
    }
    backToMain();
  }
}
