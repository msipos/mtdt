package mtdt;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

class JHPanel extends JPanel {
  private static final long serialVersionUID = 1L;


  public JHPanel() {
    final BoxLayout bl = new BoxLayout(this, BoxLayout.X_AXIS);
    setLayout(bl);
  }


  public void addStrut(int pixels) {
    add(Box.createHorizontalStrut(pixels));
  }


  public void addGlue() {
    add(Box.createHorizontalGlue());
  }
}
