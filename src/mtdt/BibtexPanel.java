package mtdt;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StringReader;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXParser;
import org.jbibtex.Key;
import org.jbibtex.Value;

public class BibtexPanel {
  public final JVPanel panel;
  public final JTextArea textArea;
  private final JHPanel bottomPanel, textPanel;
  private final JButton loadButton, cancelButton;
  private final JScrollPane jsp;

  BibtexPanel() {
    panel = new JVPanel();
    panel.addStrut(16);
    panel.add(new JLabel("Paste BibTex information here:"));
    textPanel = new JHPanel();
    textPanel.addStrut(16);
    textArea = new JTextArea();
    jsp = new JScrollPane(textArea);
    jsp.setPreferredSize(new Dimension(800, 1200));
    textPanel.add(jsp);
    textPanel.addStrut(16);
    panel.add(textPanel);
    panel.addStrut(8);
    bottomPanel = new JHPanel();
    bottomPanel.addGlue();
    cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { actionCancel(); }});
    bottomPanel.add(cancelButton);
    bottomPanel.addStrut(8);
    loadButton = new JButton("Load BibTex");
    loadButton.addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { actionLoadBibtex(); }});
    bottomPanel.add(loadButton);
    bottomPanel.addGlue();
    panel.add(bottomPanel);
    panel.addStrut(16);
  }

  private void actionCancel() {
    textArea.setText(null);
    Mtdt.mw.switchToProp();
  }

  private void actionLoadBibtex() {
    final String bibtex = textArea.getText();
    textArea.setText(null);
    final BibTeXParser parser = new BibTeXParser();
    try {
      final BibTeXDatabase db = parser.parse(new StringReader((bibtex)));
      final Map<Key, BibTeXEntry> mapEntries = db.getEntries();

      if (mapEntries.size() != 1) {
        Mtdt.mw.warning("More than one bibtex entry!");
        return;
      }

      for (Key entryKey : mapEntries.keySet()) {
        final BibTeXEntry entry = mapEntries.get(entryKey);
        final Map<Key, Value> mapFields = entry.getFields();

        Mtdt.mw.deactivatePropTableChanges();
        Mtdt.mw.updateProperty("bibtex-type", entry.getType().getValue());
        Mtdt.mw.updateProperty("bibtex-key", entryKey.getValue());

        for (Key fieldKey : mapFields.keySet()) {
          final Value field = mapFields.get(fieldKey);
          Mtdt.mw.updateProperty("bibtex-" + fieldKey, field.toUserString());
        }
      }
      Mtdt.mw.activatePropTableChanges();
      Actions.propTableChanged();
      Mtdt.mw.switchToProp();

    } catch (Exception ex) {
      Mtdt.mw.warning(String.format("Error while parsing BibTex:%n%s", ex));
    }
  }
}
