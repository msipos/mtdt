package mtdt;

import java.util.prefs.Preferences;
public class MtdtPrefs {
  private final Preferences prefs;


  public String getLastDb() { return prefs.get("LastDb", null); }
  public void setLastDb(String lastDb) { prefs.put("LastDb", lastDb); }


  public int getWidth() { return prefs.getInt("width", 300); }
  public void setWidth(int width) { prefs.putInt("width", width); }
  public int getHeight() { return prefs.getInt("height", 300); }
  public void setHeight(int height) { prefs.putInt("height", height); }


  public int getX() { return prefs.getInt("x", -1); }
  public void setX(int x) { prefs.putInt("x", x); }


  public int getY() { return prefs.getInt("y", -1); }
  public void setY(int y) { prefs.putInt("y", y); }


  public boolean getMaximizedH() { return prefs.getBoolean("maximizedh", false); }
  public void setMaximizedH(boolean b) { prefs.putBoolean("maximizedh", b); }


  public boolean getMaximizedV() { return prefs.getBoolean("maximizedv", false); }
  public void setMaximizedV(boolean b) { prefs.putBoolean("maximizedv", b); }


  public MtdtPrefs() {
    prefs = Preferences.userNodeForPackage(this.getClass());
  }
}
