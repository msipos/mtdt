package mtdt.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

public final class Item implements Serializable, Comparable<Item> {
  private static final long serialVersionUID = 18580423L;


  private final String key;
  public String getKey() { return key; }
  private final String displayName;


  private transient String lcKey, lcDisplayName;


  private final String[] props;
  public String[] getProps() { return props; }
  private final String[] values;
  public String[] getValues() { return values; }


  /** For now, this is a hack to make sure deserialization works properly. Better solution is to overwrite the
   * deserialization mechanism. */
  public void init() {
    lcKey = key.toLowerCase();
    lcDisplayName = displayName.toLowerCase();
  }


  Item(Item other, String[] props, String[] values) {
    this.key = other.key;
    this.displayName = other.displayName;
    init();

    this.props = props;
    this.values = values;
  }


  Item(String key, String displayName) {
    this.key = key;
    this.displayName = displayName;
    init();

    props = new String[] { "tags" };
    values = new String[] { "" };
  }


  public Object readResolve() {
    init();
    return this;
  }


  @Override public String toString() {
    return displayName;
  }


  @Override public int compareTo(Item o) {
    return lcDisplayName.compareTo(o.lcDisplayName);
  }


  public boolean match(ArrayList<String> matchers) {
    matcherLoop:
    for (String matcher : matchers) {
      if (lcKey.contains(matcher)) continue matcherLoop;

      if (matcher.equals("untagged")) {
        for (int i = 0; i < props.length; i++) {
          final String prop = props[i];
          if (prop.equals("tags")) {
            final String value = values[i];
            if (value.trim().equals("")) {
              continue matcherLoop;
            } else {
              return false;
            }
          }
        }
      }

      for (String value : values) {
        if (value != null && value.contains(matcher)) continue matcherLoop;
      }

      return false;
    }
    return true;
  }


  public File getFile() {
    return new File(Model.getRootDir() + File.separator + key);
  }


  public String get(String prop) {
    int counter = 0;
    for (String p : props) {
      if (prop.equals(p)) {
        return values[counter];
      }
      counter++;
    }
    return null;
  }


  public String getBibtex(String prop) {
    return get("bibtex-" + prop);
  }
}
