package mtdt.model;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import jdbm.PrimaryHashMap;
import jdbm.RecordManager;
import jdbm.RecordManagerFactory;

/** Data model is the following:
 *
 * Set of Item objects which are only mutable through the Item methods.
 * If an Item needs to be added (by the Scanner usually), then it uses a newItem() method.
 * All of these methods keep the DB in sync. */
public class Model {
  private static RecordManager rman = null;
  private static PrimaryHashMap<String, Item> items = null;
  private static String rootDir = null;
  private static String filename = null;
  public static String getRootDir() {
    return rootDir;
  }
  private static Item currentItem = null;
  public static Item getCurrentItem() { return currentItem; }
  public static void setCurrentItem(Item item) { currentItem = item; }
  public static void changeCurrentItemsProps(final String[] props, final String[] values) {
    assert currentItem != null;

    final Item newItem = new Item(currentItem, props, values);
    currentItem = newItem;
    items.put(newItem.getKey(), newItem);
  }
  public static void deleteCurrentItem() {
    items.remove(currentItem.getKey());
    currentItem = null;
  }


  public static void init(String rootDir) throws IOException {
    finish();

    Model.rootDir = rootDir;
    filename = new File(new File(rootDir), "mtdt").getAbsolutePath();
    System.out.println("Opening the database: " + filename);
    rman = RecordManagerFactory.createRecordManager(filename);
    items = rman.hashMap("items");
  }


  public static Item newItem(String key, String displayName) {
    final Item newItem = new Item(key, displayName);
    items.put(key, newItem);
    return newItem;
  }


  public static Item getItemByKey(String key) throws IOException {
    return items.find(key);
  }


  public static void commit() throws IOException {
    rman.commit();
  }


  public static Collection<Item> getAllItems() {
    return items.values();
  }


  /** Close the model Db if it is opened. Can be called on any thread. */
  public static void finish() {
    if (rman != null) {
      System.out.println("Closing the database: " + filename);
      try {
        rman.close();
      } catch (IOException ex) {
        /* Ignore. Nothing we can do about this exception. If the user can see stderr, let's warn them. */
        System.err.println("Warning: There has been an IO error while attempting to close the database: "
                          + ex.getMessage());
      }
    }
  }
}
