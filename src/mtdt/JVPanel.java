package mtdt;


import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;


public class JVPanel extends JPanel {
  private static final long serialVersionUID = 1L;


  public JVPanel() {
    final BoxLayout bl = new BoxLayout(this, BoxLayout.Y_AXIS);
    setLayout(bl);
  }


  public void addStrut(int pixels) {
    add(Box.createVerticalStrut(pixels));
  }


  public void addGlue() {
    add(Box.createVerticalGlue());
  }
}
