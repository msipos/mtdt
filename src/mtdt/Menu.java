package mtdt;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import mtdt.model.Item;
import mtdt.model.Model;
class Menu {
  public final JMenuBar menuBar = new JMenuBar();

  Menu() {
    final JMenu fileMenu = new JMenu("File");
    menuBar.add(fileMenu);

    final JMenuItem newDbItem = new JMenuItem("Create/open database...");
    newDbItem.addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { actionNewDb(); }});
    fileMenu.add(newDbItem);

    final JMenuItem scanDbItem = new JMenuItem("Scan folder");
    scanDbItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
    scanDbItem.addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { actionScanDb(); }});
    fileMenu.add(scanDbItem);

    fileMenu.addSeparator();

    final JMenuItem quitItem = new JMenuItem("Quit");
    quitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
    quitItem.addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { actionQuit(); }});
    fileMenu.add(quitItem);

    final JMenu itemMenu = new JMenu("Item");
    menuBar.add(itemMenu);

    final JMenuItem newItem = new JMenuItem("New item");
    newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
    newItem.addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { actionNewItem(); }});
    itemMenu.add(newItem);

    itemMenu.addSeparator();

    final JMenuItem copyBibtexItem = new JMenuItem("Copy bibtex");
    copyBibtexItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
    copyBibtexItem.addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { actionCopyBibtex(); }});
    itemMenu.add(copyBibtexItem);

    final JMenuItem copyCitationItem = new JMenuItem("Copy citation");
    copyCitationItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
    copyCitationItem.addActionListener(new ActionListener() { @Override public void actionPerformed(ActionEvent e) { actionCopyCitation(); }});
    itemMenu.add(copyCitationItem);
  }

  private void actionNewDb() {
    int val = Mtdt.mw.fileChooser.showOpenDialog(Mtdt.mw);
    if (val == JFileChooser.APPROVE_OPTION) {
      final File rootDir = Mtdt.mw.fileChooser.getSelectedFile();
      Actions.newDb(rootDir);
    } else {
      /* Ignore. */
    }
  }

  private void actionScanDb() {
    Actions.scanDb();
  }

  private void actionQuit() {
    Mtdt.mw.dispose();
  }

  private void actionCopyBibtex() {
    Collection<Item> items = Mtdt.mw.getSelectedItems();

    final StringBuilder sb = new StringBuilder();
    int added = 0;

    for (final Item i : items) {
      final String bibtexType = i.getBibtex("type");
      final String bibtexKey = i.getBibtex("key");
      if (bibtexKey == null || bibtexType == null) continue;

      final ArrayList<String> stuff = new ArrayList<String>();
      int count = 0;
      for (String prop : i.getProps()) {
        if (prop.startsWith("bibtex-")) {
          final String key = prop.substring(7);
          if (key.equals("key")) continue;
          if (key.equals("type")) continue;

          stuff.add(key);
          stuff.add(i.getBibtex(key));
        }
        count++;
      }
      if (added > 0) sb.append("\n");
      sb.append(BibtexHelper.toBibtex(bibtexType, bibtexKey, stuff));
      added++;
    }

    SUtil.setClipboard(sb.toString());
  }

  private void actionCopyCitation() {
    Collection<Item> items = Mtdt.mw.getSelectedItems();

    final StringBuilder sb = new StringBuilder("\\cite{");
    int added = 0;

    for (final Item i : items) {
      final String bibtexKey = i.getBibtex("key");
      if (bibtexKey == null) continue;
      if (added > 0) sb.append(",");
      sb.append(bibtexKey);
      added++;
    }
    sb.append("}");

    SUtil.setClipboard(sb.toString());
  }

  private void actionNewItem() {
    final String itemName = JOptionPane.showInputDialog(Mtdt.mw, "Enter item's display name. Item will be without a file reference.");
    if (itemName == null) {
      // User clicked cancel, nothing to do.
      return;
    }
    final String name = itemName.trim();

    if (name.trim().equals("")) {
      Mtdt.mw.warning("Must have a non-whitespace character in the item name.");
      return;
    }

    Model.newItem(name, name);
    try {
      Model.commit();
    } catch (IOException ex) {
      Mtdt.mw.warning("IO error with the database: " + ex);
      return;
    }
    Actions.updateListModel();
  }
}
