package mtdt;

// TODO:

import java.awt.Frame;
import java.io.File;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Mtdt {
  static MainWindow mw = null;
  static MtdtPrefs prefs = new MtdtPrefs();


  public static void main(String[] args) {
    UIManager.put("swing.boldMetal", Boolean.FALSE);
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Metal".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
    } catch (InstantiationException ex) {
    } catch (IllegalAccessException ex) {
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {}

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      @Override public void run() {
        mw = new MainWindow();
        mw.setLocationByPlatform(true);
      }
    });


    final String lastDb = prefs.getLastDb();
    if (lastDb != null) {
      SwingUtilities.invokeLater(new Runnable() { @Override public void run() {
        Actions.newDb(new File(lastDb));
        Actions.updateListModel();
      }});
    }

    SwingUtilities.invokeLater(new Runnable() { @Override public void run() {
      if (prefs.getX() >= 0 && prefs.getY() >= 0) {
        mw.setLocation(prefs.getX(), prefs.getY());
      }
      mw.setSize(prefs.getWidth(), prefs.getHeight());
      if (prefs.getMaximizedH() && prefs.getMaximizedV()) {
        mw.setExtendedState(Frame.MAXIMIZED_BOTH);
      } else if (prefs.getMaximizedH()) {
        mw.setExtendedState(Frame.MAXIMIZED_HORIZ);
      } else if (prefs.getMaximizedV()) {
        mw.setExtendedState(Frame.MAXIMIZED_VERT);
      }
      mw.setVisible(true);
    }});
  }
}
