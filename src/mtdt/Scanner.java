package mtdt;

import java.io.File;
import java.io.IOException;
import mtdt.model.Item;
import mtdt.model.Model;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.EmptyFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class Scanner {
  public static void scan(File rootFile) throws IOException {
    final String rootPath = rootFile.getAbsolutePath();

    int numExisting = 0;
    int numNew = 0;
    for (File f : FileUtils.listFiles(rootFile, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE)) {
      final String relativePath = f.getAbsolutePath().substring(rootPath.length() + 1);
      final String displayPath = f.getName();

      // Ignore mtdt databases:
      if (relativePath.startsWith("mtdt") || displayPath.startsWith("mtdt")) continue;

      final String key = relativePath;
      final Item existingItem = Model.getItemByKey(key);

      if (existingItem == null) {
        numNew++;
        Model.newItem(key, displayPath);
      } else {
        numExisting++;
      }
    }

    Model.commit();
    System.out.println("File scan complete: new items: " + numNew + " existing items: " + numExisting);
  }
}
