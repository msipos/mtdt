package mtdt;

import java.util.ArrayList;

public class BibtexHelper {
  /** Items is [key, value, key, value, key, value...] */
  public static String toBibtex(String type, String key, ArrayList<String> items) {
    final StringBuilder sb = new StringBuilder("@" + type + "{" + key);

    for (int i = 0; i < items.size() / 2; i++) {

      sb.append(",\n  ");
      sb.append(items.get(2*i));
      sb.append("={");
      sb.append(items.get(2*i+1));
      sb.append("}");
    }
    sb.append("\n}\n");
    return sb.toString();
  }
}
