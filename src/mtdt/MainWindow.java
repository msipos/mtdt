package mtdt;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListModel;
import javax.swing.border.EmptyBorder;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import mtdt.Actions;
import mtdt.Mtdt;
import mtdt.MtdtPrefs;
import mtdt.model.Item;
import mtdt.model.Model;
import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXParser;
import org.jbibtex.Key;
import org.jbibtex.ParseException;
import org.jbibtex.Value;

public class MainWindow extends JFrame implements WindowListener, ActionListener, ComponentListener, WindowStateListener {
  private static final long serialVersionUID = 1L;


  // Condition:
  private MainWindowCondition condition = MainWindowCondition.LIST;
  MainWindowCondition getCondition() {
    return condition;
  }
  void switchToList() {
    SUtil.switchCard(rootPanel, "mainCard");
    condition = MainWindowCondition.LIST;
  }
  void switchToProp() {
    SUtil.switchCard(rootPanel, "subCard");
    SUtil.switchCard(subSubPanel, "propCard");
    condition = MainWindowCondition.PROPERTY;
  }
  public void switchToBib() {
    SUtil.switchCard(subSubPanel, "bibCard");
    condition = MainWindowCondition.BIBTEX;
  }


  // Components:
  JFileChooser fileChooser = new JFileChooser();
  {
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
  }
  private JPanel rootPanel = new JPanel(new CardLayout());
  private JPanel messagePanel = new JPanel();
  {
    BoxLayout bl = new BoxLayout(messagePanel, BoxLayout.Y_AXIS);
    messagePanel.setLayout(bl);
  }
  private final JPanel mainPanel = new JPanel();
  private final JList<Item> items = new JList<Item>();
  private final DefaultListModel<Item> itemsModel = new DefaultListModel<Item>();
  {
    items.setModel(itemsModel);
  }
  public DefaultListModel<Item> getItemListModel() { return itemsModel; }
  private final JTextField searchField = new JTextField();
  {
    searchField.addActionListener(this);
    searchField.setActionCommand("Search");
  }
  private final JPanel subPanel = new JPanel();
  private final DefaultTableModel propTableModel = new DefaultTableModel();
  private final JTable propTable = new JTable(propTableModel);
  public DefaultTableModel getPropTableModel() { return propTableModel; }
  private boolean propTableListen = false;
  private final TableModelListener propTableListener = new TableModelListener() {
    @Override public void tableChanged(TableModelEvent e) {
      if (propTableListen) Actions.propTableChanged();
    }
  };
  {
    propTableModel.addTableModelListener(propTableListener);
  }
  void activatePropTableChanges() {
    propTableListen = true;
  }
  void deactivatePropTableChanges() {
    propTableListen = false;
  }
  private final JLabel subtitleLabel = new JLabel();
  {
    subtitleLabel.setMinimumSize(new Dimension(30, 14));
  }
  public void setSubtitle(String subtitle) { subtitleLabel.setText(subtitle); }
  private JPanel subSubPanel = new JPanel(new CardLayout());


  private static void setHorizontalBoxLayout(JPanel panel) {
    final BoxLayout bl = new BoxLayout(panel, BoxLayout.X_AXIS); panel.setLayout(bl);
  }
  private static void setVerticalBoxLayout(JPanel panel) {
    final BoxLayout bl = new BoxLayout(panel, BoxLayout.Y_AXIS); panel.setLayout(bl);
  }
  private static void pad(JComponent comp) {
    comp.setBorder(new EmptyBorder(8, 8, 8, 8));
  }


  /** This returns a collection of currently selected items (possibly empty).  If in active property view, then the
   * selected item is the current item. */
  Collection<Item> getSelectedItems() {
    if (condition == MainWindowCondition.LIST) {
      return items.getSelectedValuesList();
    } else {
      final ArrayList<Item> items = new ArrayList<Item>();
      items.add(Model.getCurrentItem());
      return items;
    }
  }


  /** Find the given named key among the properties and change it to value.  If no such key, then add a new row and
   * make it. */
  void updateProperty(String key, String value) {
    // TODO: Look for row.

    propTableModel.addRow(new String[] { key, value } );
  }

  private final BibtexPanel bibtexPanel;
  private final Menu menu;


  public MainWindow() {
    // General stuff for the root
    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    Container contentPane = getContentPane();
    contentPane.setPreferredSize(new Dimension(400, 500));
    contentPane.add(rootPanel);

    // Populate "messageCard"
    rootPanel.add(messagePanel, "messageCard");
    {
      JLabel lbl = new JLabel("Create or open a database to get started");
      lbl.setAlignmentX(0.5f);
      messagePanel.add(Box.createVerticalGlue()); // To vertically align the lbl
      messagePanel.add(lbl);
      messagePanel.add(Box.createVerticalGlue());
    }

    // Populate "mainCard"
    rootPanel.add(mainPanel, "mainCard");
    setHorizontalBoxLayout(mainPanel);
    {
      final JPanel jp1 = new JPanel(); setVerticalBoxLayout(jp1);

      // Items:
      final JScrollPane jsp = new JScrollPane(items);
      jsp.setMinimumSize(new Dimension(150, 100));
      jsp.setPreferredSize(new Dimension(500, 700));
      jsp.setMaximumSize(new Dimension(4000, 5000));
      jp1.add(jsp);

      final MouseListener mouseListener = new MouseAdapter() { @Override public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          int index = items.locationToIndex(e.getPoint());
          Actions.pickedListItem(index);
        }
      }};
      items.addMouseListener(mouseListener);

      jp1.add(Box.createVerticalStrut(8));

      // Search box:
      final JPanel jp2 = new JPanel(); setHorizontalBoxLayout(jp2);
      jp2.add(new JLabel("Search:"));
      jp2.add(Box.createHorizontalStrut(8));
      jp2.add(searchField);
      jp1.add(jp2);

      mainPanel.add(jp1); pad(mainPanel);
    }
    mainPanel.setMinimumSize(new Dimension(200, 200));

    // Populate "subCard"
    rootPanel.add(subPanel, "subCard");
    setVerticalBoxLayout(subPanel);
    pad(subPanel);
    {
      // Create topPanel:
      final JPanel topPanel = new JPanel(); setHorizontalBoxLayout(topPanel);

      final JButton addRow = SUtil.newButton("Add row", this, "AddRow");
      addRow.setMinimumSize(new Dimension(60, 14));
      final JButton removeRow = SUtil.newButton("Remove row", this, "RemoveRow");
      removeRow.setMinimumSize(new Dimension(60, 14));

      topPanel.add(subtitleLabel);
      topPanel.add(Box.createHorizontalGlue());
      topPanel.add(addRow);
      topPanel.add(Box.createHorizontalStrut(8));
      topPanel.add(removeRow);
      subPanel.add(topPanel);

      // Separator:
      subPanel.add(Box.createVerticalStrut(8));

      // Table:
      propTableModel.addColumn("Property");
      propTableModel.addColumn("Value");

      // Scroll pane:
      final JScrollPane jsp = new JScrollPane(propTable);
      jsp.setMinimumSize(new Dimension(150, 100));
      jsp.setPreferredSize(new Dimension(500, 700));
      jsp.setMaximumSize(new Dimension(4000, 5000));

      // BibTex form:
      bibtexPanel = new BibtexPanel();

      // SubSubPanel:
      subPanel.add(subSubPanel);
      subSubPanel.add(jsp, "propCard");
      subSubPanel.add(bibtexPanel.panel, "bibCard");

      // Separator:
      subPanel.add(Box.createVerticalStrut(8));

      // Create bottomPanel:
      final JPanel bottomPanel = new JPanel(); setHorizontalBoxLayout(bottomPanel);
      subPanel.add(bottomPanel);

      bottomPanel.add(SUtil.newButton("Back", this, "Back"));
      bottomPanel.add(Box.createHorizontalStrut(8));
      bottomPanel.add(SUtil.newButton("Enter BibTex", this, "EnterBibtex"));
      bottomPanel.add(Box.createHorizontalStrut(8));
      bottomPanel.add(SUtil.newButton("Delete", this, "Delete"));
      bottomPanel.add(Box.createHorizontalGlue());
      bottomPanel.add(SUtil.newButton("Show", this, "Show"));
    }

    // Populate menu:
    menu = new Menu();
    setJMenuBar(menu.menuBar);

    // Global window stuff:
    addWindowListener(this);
    addWindowStateListener(this);
    setMinimumSize(new Dimension(200, 250));
    setPreferredSize(new Dimension(400, 500));
    setTitle("MTDT");
    pack();
    addComponentListener(this);
  }


  @Override public void windowClosing(WindowEvent e) {
    Model.finish();
  }


  @Override public void actionPerformed(ActionEvent e) {
    final String command = e.getActionCommand();

    if (command.equals("Search")) {
      Actions.updateListModel();
    } else if (command.equals("Back")) {
      Actions.backToMain();
    } else if (command.equals("Show")) {
      Actions.showCurrentItem();
    } else if (command.equals("AddRow")) {
      propTableModel.addRow(new String[] { "", "" } );
    } else if (command.equals("RemoveRow")) {
      int[] selectedRows = propTable.getSelectedRows();
      Arrays.sort(selectedRows);
      for (int i = selectedRows.length - 1; i >= 0; i--) {
        int row = selectedRows[i];
        propTableModel.removeRow(row);
      }
    } else if (command.equals("Delete")) {
      if (JOptionPane.showConfirmDialog(this, "Delete item '" + Model.getCurrentItem().getKey() + "'?",
                                        "Really delete?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
        Actions.delete();
      }
    } else if (command.equals("EnterBibtex")) {
      switchToBib();
    } else {
      System.err.println("ERROR: This should not happen.  This is a programmer error.  Unknown command: " + command);
    }
  }


  public void warning(String text) {
    JOptionPane.showMessageDialog(this, text, "Warning", JOptionPane.WARNING_MESSAGE);
  }


  public String getSearchText() {
    return searchField.getText();
  }


  /** Keep record of size: */
  @Override public void componentResized(ComponentEvent e) {
    if ((getExtendedState() & Frame.MAXIMIZED_VERT) != Frame.MAXIMIZED_VERT) {
      if ((getExtendedState() & Frame.MAXIMIZED_HORIZ) != Frame.MAXIMIZED_HORIZ) {
        final int width = getSize().width, height = getSize().height, x = getLocation().x, y = getLocation().y;

        Mtdt.prefs.setX(x); Mtdt.prefs.setY(y); Mtdt.prefs.setWidth(width); Mtdt.prefs.setHeight(height);
      }
    }
  }


  /** Keep record of position: */
  @Override public void componentMoved(ComponentEvent e) {
    if ((getExtendedState() & Frame.MAXIMIZED_VERT) != Frame.MAXIMIZED_VERT) {
      if ((getExtendedState() & Frame.MAXIMIZED_HORIZ) != Frame.MAXIMIZED_HORIZ) {
        final int width = getSize().width, height = getSize().height, x = getLocation().x, y = getLocation().y;

        Mtdt.prefs.setX(x); Mtdt.prefs.setY(y); Mtdt.prefs.setWidth(width); Mtdt.prefs.setHeight(height);
      }
    }
  }


  /** Keep record of maximized state: */
  @Override public void windowStateChanged(WindowEvent e) {
    if ((e.getNewState() & Frame.MAXIMIZED_HORIZ) == Frame.MAXIMIZED_HORIZ) {
      Mtdt.prefs.setMaximizedH(true);
    } else {
      Mtdt.prefs.setMaximizedH(false);
    }
    if ((e.getNewState() & Frame.MAXIMIZED_VERT) == Frame.MAXIMIZED_VERT) {
      Mtdt.prefs.setMaximizedV(true);
    } else {
      Mtdt.prefs.setMaximizedV(false);
    }
  }


  // Useless boilerplate:
  @Override public void componentShown(ComponentEvent e) { }
  @Override public void componentHidden(ComponentEvent e) { }
  @Override public void windowOpened(WindowEvent e) { }
  @Override public void windowClosed(WindowEvent e) { }
  @Override public void windowIconified(WindowEvent e) { }
  @Override public void windowDeiconified(WindowEvent e) { }
  @Override public void windowActivated(WindowEvent e) { }
  @Override public void windowDeactivated(WindowEvent e) { }
}
