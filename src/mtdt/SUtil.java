package mtdt;


import java.awt.CardLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;


/** A few repeated Swing code. */
public class SUtil {
  public static void switchCard(JPanel jp, String name) {
    CardLayout cl = (CardLayout) jp.getLayout();
    cl.show(jp, name);
  }


  public static JButton newButton(String label, ActionListener actionListener, String actionCommand) {
    final JButton button = new JButton(label);
    button.addActionListener(actionListener);
    button.setActionCommand(actionCommand);
    return button;
  }


  public static void setClipboard(String text) {
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(text), null);
    System.out.println("Setting clipboard to:");
    System.out.println(text);
  }
}
